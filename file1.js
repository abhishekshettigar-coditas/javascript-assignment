console.log("Hello World");

let name = "    Abhishek   ";
let newName = name.trim();
console.log(newName);

let upperCaseName = name.toUpperCase();
console.log(upperCaseName);

let lowerCaseName = name.toLowerCase();
console.log(lowerCaseName);

let sliceName = newName.slice(0,4);
console.log(sliceName);


// convert number to string

num = 21;
console.log(typeof num);

newNum = num + "";
console.log(newNum);   //"22"
console.log(typeof newNum);


// convert string to number

myStr = "22";

myNum = +myStr; // "22" to 22
console.log(myNum);
console.log(typeof myNum);


// Number(str) convert string to number
// String(num) convert number to string


// STRING CONCATENATION

let str1 = "Abhishek";
let str2 = "Shettigar";
let age = 21;

let fullName = str1 + " " + str2;
console.log(fullName);


// TEMPLATE STRING

myName = `My nam is ${fullName} and my age is ${age}`;
console.log(myName);

// num and null
let nuum;
console.log(nuum);

let variable = null;
console.log(variable);
console.log(typeof variable);


// BigInt

let myNum = BigInt(287464745838437438473987);   //can also can be written as 287464745838437438473987n
console.log(myNum);