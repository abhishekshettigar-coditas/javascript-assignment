// PRIMITIVE DATA TYPES

let  num1= 6;
let num2 = num1;
console.log("num1 is ", num1);
console.log("num2 is ", num2);
num1++;

console.log("num1 is ", num1);
console.log("num2 is ", num2);

// REFERENCE DATA TYPES

let arr1 = ["item1", "item2"];
let arr2 = arr1;
arr1.push("item3");
console.log(arr1);
console.log(arr2);


let array1 = [1,2,3,4,5];
let array2 = [...array1];
console.log(array2);


