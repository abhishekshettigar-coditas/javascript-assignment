const key = "email";
const person = {
    "name" : "harshit",
    age: 22,
    hobbies : ["eating", "sleeping","playing" ]
}

console.log(person);
console.log(person.name);
console.log(person.age);

 person["place"] = "Mangalore";
 console.log(person);
person[key] = "xyz@gmail.com"
console.log(person);


// for in loop

for(let key in person){
    console.log(key);
}

for(let key in person){
    console.log(person[key]);
}

for(let key in person){
    console.log(`${key} : ${person[key]}`);
}


const key1 = "objkey1";
const key2 = "objkey2";

const value1 = "myvalue1";
const value2 = "myvalue2";

// const obj = {
//     [key1] : value1,
//     [key2] : value2
// }

// or

const obj = {};

obj[key1]= value1;
obj[key2]= value2;

console.log(obj);


// object destructuring

const band = {
    bandName : "led zepplin",
    famousSong : "stairway to heaven",
    year : 1962,
    anotherFamousSong: "kashmir"
};

let { bandName, famousSong, ...restProps} = band;

console.log(bandName);
console.log(restProps);

// nested object in array
const users = [  
    {userId : 1, firstName: "Abhishek", gender: "male"},
    {userId : 2, firstName: "Mohit", gender: "male"},
    {userId : 3, firstName: "Rachel", gender: "female"},
];

for (let user of users){
    console.log(user);
}
for (let user of users){
    console.log(user.userId);
}
for (let user of users){
    console.log(user.firstName);
}
for (let user of users){
    console.log(user.gender);
}

// nested destructuring

const [user1, user2, user3] = users;
console.log(user1);

const [{firstName},{userId}, {gender}] = users;

console.log(firstName);
console.log(userId);
console.log(gender);



